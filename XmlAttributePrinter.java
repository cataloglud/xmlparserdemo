import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;


public class XmlAttributePrinter {
	static String mySourcePath ="testFiles/xmlfeed.xml"; // input file source path
	static String myElement = "reference"; // the Node that has the desired attribute
	static String attributeToGet = "ref_url"; // the attribute to print
	static String attributeToCheck = "source"; // has to be an attribute of same node as attributeToGet
   static String desiredSource = "RHSA"; // to check if the source is "RHSA" before printing
	
	
	public static void main(String[] args) {
	   
      try {
         File inputFile = new File(mySourcePath);
         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
         Document doc = dBuilder.parse(inputFile);
         doc.getDocumentElement().normalize();
         NodeList nList = doc.getElementsByTagName(myElement);
         
         
         for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
                       
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
               Element eElement = (Element) nNode;
               if(eElement.getAttribute(attributeToCheck).equals(desiredSource)){
            	   
            	   System.out.println(eElement.getAttribute(attributeToGet));
            	   
              }
            }
         }
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }
   }
}
